import React, { useState } from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
  const [count, setCount] = useState(0);
  const [text, setText] = useState(POOP_TEXT);

  function handleIncrement() {
    setCount((cnt) => cnt + 1);
    textValue(count + 1);
  }

  function handleDecrement() {
    if (count > 0) {
      setCount((cnt) => cnt - 1);
      textValue(count - 1);
    }
  }

  function textValue(count) {
    if (count === 0) {
      return setText(POOP_TEXT);
    } else if (count % 3 === 0 && count % 5 === 0) {
      return setText(COUNTER_FOO_TEXT);
    } else if (count % 3 === 0) {
      return setText(COUNTER_TEXT);
    } else if (count % 5 === 0) {
      return setText(FOO_TEXT);
    } else if (count % 3 !== 0 && count % 5 !== 0) {
      return setText(POOP_TEXT);
    }
  }

  return (
    <div style={styles.app}>
      <h1>{count}</h1>
      <h3>{text}</h3>
      <div style={styles.buttons}>
        <button onClick={handleIncrement}>Increment</button>
        <button onClick={handleDecrement}>Decrement</button>
      </div>
    </div>
  );
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
